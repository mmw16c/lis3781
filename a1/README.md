> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# Advanced Database Management

## Matthew Weas

### Assignment 1 Requirements:

*Sub-Heading:*

1. Distributed Version Control Setup
2. Development Installation
3. MySQL Server

#### README.md file should include the following items:

* Screenshot of ampps installation running
* Git commands w/short descriptions
* An ERD Image
* Bitbucket repo Links

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - Create an empty Git repository or reinitialize an existing one
2. git status - Show the working tree status
3. git add - Add file contents to the index
4. git commit - Record changes to the repository
5. git push - Update remote refs along with associated objects
6. git pull - Fetch from and integrate with another repository or a local branch
7. git branch - List all the branches in your repo, and also tell you what branch you're currently in

#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost*:

![AMPPS Installation Screenshot](img/ampps.png)

*Screenshot of ERD*:

![JDK Installation Screenshot](img/erd.png)




#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://confluence.atlassian.com/bitbucket/create-a-git-repository-759857290.html "Bitbucket Station Locations")


